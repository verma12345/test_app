import React from 'react'
const Greet = (props) => {
    return (
        <div>
            <p> 
                {props.name}
                {props.children}
            </p>
        </div>
    )
}
export default Greet;