import React from 'react';
import logo from './logo.svg';
import './App.css';
import Greet from './components/Greet';
import GreetClass from './class_components/GreetClass';

function App() {
  return (
    // hey jazzz
    <div className="App">
      <Greet
        name="welcome"
      >
        <p>
          Calling Greet in App.js
        </p>
      </Greet  >
      <GreetClass />
    </div>
  );
}

export default App;
